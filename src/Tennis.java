import java.util.HashMap;
import java.util.Map;


public class Tennis {

	//Map<int,String> results = new Map();
	Map<Integer, String> results = new HashMap<Integer, String>();
	int score1=0,score2=0;
	String current_score;
	
	public Tennis(){
		results.put(new Integer(0),"Love");
		results.put(new Integer(1),"Fifteen");
		results.put(new Integer(2),"Thirty");
		results.put(new Integer(3),"Fourty");
		
	}
	public void wonPoint(String player) {
		if (player.equals("player1")) {
			++score1;
		} else {			
			++score2;
		}
	}
	public String getScore() {
		String med = "Advantage Player";
		String equ = "Deuce";
		
		if( score1 != score2 ){
			
			if (score1 > score2) {
				if (score1 - score2 == 1 && score1+score2>6) {
					current_score = med +"1";
				} else {
					current_score = results.get(score1) + " - " +results.get(score2);
				}
			}

			if (score2 > score1) {
			
				if(score2 - score1 == 1  && score1+score2>6) {
					current_score = med +"2";
				}  else {
					current_score = results.get(score1) + " - " +results.get(score2);
				}
				
			}
			
			
			
		} 
		if (score1 == score2 && (score1 == 3)) {
			current_score = equ;
		} else {
			current_score = results.get(score1) + " - " +results.get(score2);
		}
		if(score1 == score2 && (score1+score2 > 7)) {
			current_score = equ;
		}
		
		return current_score;
	}
	public void reset(){
		score1=0;
		score2=0;
	}

}
